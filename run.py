
import time
import sys

from matplotlib import pyplot as plt
from fractals import make_fractal

import argparse

parser = argparse.ArgumentParser(description='Short sample app')

parser.add_argument('--model', default="julia")
parser.add_argument('--size', type=int, default=256)
parser.add_argument('--depth', type=int, default=64)
parser.add_argument('--zoom', type=float, default=1.0)
parser.add_argument('--center', nargs=2, type=float, default=(0.0, 0.0))
parser.add_argument('--colormap', default="cubehelix")
#TODO: zoom
#TODO: center x, y
#TODO: some other? colormap?

args = parser.parse_args()

# Do main work
start = time.perf_counter()

img = make_fractal(model=args.model,
                   size=(args.size, args.size),
                   depth=args.depth,
                   zoom=args.zoom,
                   center=args.center)
end = time.perf_counter()

print('Elapsed time (in seconds): {}'.format(end - start))
plt.imshow(img, args.colormap)
plt.show()